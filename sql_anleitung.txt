Daten:
Datenbankname: vt_bennylukas
Tabellenname: guest
id, vocab, translation, note, creationdate, genre

Beispiel:
=========
create database Vocabulary collate utf8_general_ci;

use Vocabulary;

create table words(

	id int not null auto_increment,

    	english varchar(300),

	german varchar(300),

	note varchar(400),

	date_data date,
	constraint id_pk primary key(id)

)engine=InnoDB;


insert into words values(null, "thoroughly", "gr�ndlich", "erster Eintrag 2018/19", '2018-09-10');

insert into words values(null, "bellow", "br�llen", "-", '2018-09-10');

insert into words values(null, "scream", "schreien", "-", '2018-09-10');

insert into words values(null, "shriek", "kreischen", "-", '2018-09-10');

insert into words values(null, "shout", "rufen", "-", '2018-09-10');

=========================
Funktionen: l�schen, hinzuf�gen

meine benjamin

Meine Zeile Lukas