﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Vokabeltrainer_BennyLukas.Models
{
    public class UserVocabs
    {
        public int UserID { get; set; }
        public int VocabID { get; set; }

        public UserVocabs() : this(0, 0) { }
        public UserVocabs(int uid, int vid)
        {
            this.UserID = uid;
            this.VocabID = vid;
        }
    }
}