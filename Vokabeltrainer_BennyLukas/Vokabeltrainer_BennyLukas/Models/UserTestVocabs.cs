﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Vokabeltrainer_BennyLukas.Models
{
    public class UserTestVocabs
    {
        public int UserID { get; set; }
        public int TestId { get; set; }
        public int VocabID { get; set; }


        public UserTestVocabs() : this(0,0,0) { }
        public UserTestVocabs(int uId, int tId, int vId)
        {
            this.UserID = uId;
            this.TestId = tId;
            this.VocabID = vId;
        }
    }
}