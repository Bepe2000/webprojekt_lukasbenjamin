﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Vokabeltrainer_BennyLukas.Models.db
{
    interface IRepositoryVocs
    {
        void Open();
        void Close();
        List<Word> GetAllPublicWords();
        bool Remove(int idToDelete);
        bool Insert(Word wordToInsert);

        //connection to user:
        bool InsertUserVocableRealtion(UserVocabs uvToInsert);
        bool RemoveUserVocableRelation(UserVocabs uvToRemove);
        List<Word> GetAllUserVocabs(int uidToFind);
        int GetLastVocabID();
        Word GetUserVocab(int uidToFind);   
    }
}
