use swp_vocember;
create table user_vocabs(
	userid int not null,
	vocabid int not null,
    constraint user_vocabs_pk primary key(userid, vocabid),
	constraint user_id_fk foreign key(userid) references users(id),
	constraint vocab_id_fk foreign key(vocabid) references vocabs(id)
) engine=InnoDB;

insert into user_vocabs values(1,1);
insert into user_vocabs values(1,5);
insert into user_vocabs values(1,6);
insert into user_vocabs values(1,10);
insert into user_vocabs values(1,11);

insert into user_vocabs values(2,13);
insert into user_vocabs values(2,12);

insert into user_vocabs values(4,8);
insert into user_vocabs values(4,9);

select * from user_vocabs;
