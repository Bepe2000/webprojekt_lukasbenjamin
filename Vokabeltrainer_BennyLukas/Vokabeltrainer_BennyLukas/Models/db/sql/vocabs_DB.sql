use swp_vocember;
create table vocabs(
	id int not null auto_increment,
	vocab varchar(300),
	translation varchar(300),
	note varchar(400),
	creationdate date,
    genre varchar(300),
	constraint id_pk primary key(id)
)engine=InnoDB;


insert into vocabs values(null, "alligator", "Alligator", null, '2019-03-06', "Animals");
insert into vocabs values(null, "ant", "Ameise", null, '2019-03-06', "Animals");					
insert into vocabs values(null, "bear", "Bär", null, '2019-03-06', "Animals");
insert into vocabs values(null, "bee", "Biene", null, '2019-03-06', "Animals");
insert into vocabs values(null, "cat", "Katze", null, '2019-03-06', "Animals");
insert into vocabs values(null, "cow", "Kuh", null, '2019-03-06', "Animals");

insert into vocabs values(null, "family", "Familie", null, '2019-03-06', "Family");
insert into vocabs values(null, "father", "Vater", null, '2019-03-06', "Family");
insert into vocabs values(null, "mother", "Mutter", null, '2019-03-06', "Family");
insert into vocabs values(null, "brother", "Bruder", null, '2019-03-06', "Family");
insert into vocabs values(null, "sister", "Schwester", null, '2019-03-06', "Family");
insert into vocabs values(null, "aunt", "Tante", null, '2019-03-06', "Family");
insert into vocabs values(null, "uncle", "Onkel", null, '2019-03-06', "Family");