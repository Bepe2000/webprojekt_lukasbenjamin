create database swp_vocember;
use swp_vocember;
create table users(
	id int not null auto_increment,
	firstname varchar(100) null,
	lastname varchar(100) not null,
	birthdate date null,
	gender int null,
	email varchar(100) not null,
	username varchar(100) null,
	passwrd varchar(40) not null,
    constraint id_PK primary key(id) #pk vergeben
) engine=InnoDB;

insert into users values(null,"Max","Mustermann","2000-12-14",0,"max.m@vmail.com","max2000",sha1("max2000"));