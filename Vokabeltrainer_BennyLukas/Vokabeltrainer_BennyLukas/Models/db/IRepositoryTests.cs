﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Vokabeltrainer_BennyLukas.Models.db
{
    interface IRepositoryTests
    {
        void Open();
        void Close(); 
    }
}