﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Vokabeltrainer_BennyLukas.Models.db
{
    interface IRepositoryUsers
    {
        void Open();
        void Close();
        User GetRegisteredPerson(int idToFind);
        List<User> GetAllRegisteredUsers();
        bool Remove(int idToDelete);
        bool ChangePersonData(int idToChange, User newUserData);
        User Authenticate(User userDataToLogin);
        bool Insert(User userToInsert);
    }
}
