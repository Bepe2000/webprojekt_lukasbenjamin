﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MySql.Data.MySqlClient;
using System.Data;
using Vokabeltrainer_BennyLukas.Models;

namespace Vokabeltrainer_BennyLukas.Models.db
{
    public class RepositoryUsersDB : IRepositoryUsers
    {
        private string _connectionString = "Server = localhost; Database = swp_vocember; Uid = root; Pwd = ''";
        private MySqlConnection _connection;

        public void Open()
        {
            if (this._connection == null)
            {
                this._connection = new MySqlConnection(this._connectionString);
            }
            if (this._connection.State != ConnectionState.Open)
            {
                this._connection.Open();
            }
        }

        public void Close()
        {
            if ((this._connection != null) && (this._connection.State == ConnectionState.Open))
            {
                this._connection.Close();
            }
        }

        public bool Insert(User userToInsert)
        {
            if (userToInsert == null)
            {
                return false;
            }

            try
            {
                MySqlCommand cmdInsert = this._connection.CreateCommand();
                cmdInsert.CommandText = "insert into users values(null, @firstname, @lastname, @birthdate, @gender, @email, @username, sha1(@pwd))";
                cmdInsert.Parameters.AddWithValue("firstname", userToInsert.Firstname);
                cmdInsert.Parameters.AddWithValue("lastname", userToInsert.Lastname);
                cmdInsert.Parameters.AddWithValue("birthdate", userToInsert.BirthDate);
                cmdInsert.Parameters.AddWithValue("gender", userToInsert.Gender);
                cmdInsert.Parameters.AddWithValue("email", userToInsert.EMail);
                cmdInsert.Parameters.AddWithValue("username", userToInsert.Username);
                cmdInsert.Parameters.AddWithValue("pwd", userToInsert.Password);

                return cmdInsert.ExecuteNonQuery() == 1;
            }

            catch (Exception)
            {
                throw;
            }
        }

        public List<User> GetAllRegisteredUsers()
        {
            List<User> people = new List<User>();

            try
            {
                MySqlCommand cmdAllPeople = this._connection.CreateCommand();
                cmdAllPeople.CommandText = "select * from users";

                using (MySqlDataReader reader = cmdAllPeople.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        people.Add(new User()
                        {
                            ID = Convert.ToInt32(reader["id"]),
                            Firstname = Convert.ToString(reader["firstname"]),
                            Lastname = Convert.ToString(reader["lastname"]),
                            BirthDate = reader["birthdate"] != DBNull.Value ? Convert.ToDateTime(reader["birthdate"]) : (DateTime?)null,
                            Gender = (Gender)Convert.ToInt32(reader["gender"]),
                            EMail = Convert.ToString(reader["email"]),
                            Username = Convert.ToString(reader["username"]),
                        });
                    }
                }
                return people.Count == 0 ? null : people;
            }

            catch (Exception)
            {
                throw;
            }
        }

        public User GetRegisteredPerson(int idToFind)
        {
            if (this._connection == null || this._connection.State != ConnectionState.Open)
            {
                return null;
            }

            try
            {
                MySqlCommand cmdGetRegP = this._connection.CreateCommand();
                cmdGetRegP.CommandText = "select * from users where id=@id";
                cmdGetRegP.Parameters.AddWithValue("id", idToFind);

                using (MySqlDataReader reader = cmdGetRegP.ExecuteReader())
                {
                    if (reader.HasRows)
                    {
                        reader.Read();
                        return new User
                        {
                            ID = Convert.ToInt32(reader["id"]),
                            Firstname = Convert.ToString(reader["firstname"]),
                            Lastname = Convert.ToString(reader["lastname"]),
                            BirthDate = reader["birthdate"] != DBNull.Value ? Convert.ToDateTime(reader["birthdate"]) : (DateTime?)null,
                            Gender = (Gender)Convert.ToInt32(reader["gender"]),
                            EMail = Convert.ToString(reader["email"]),
                            Username = Convert.ToString(reader["username"]),
                        };
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }
            return null;
        }

        public User Authenticate(User userDataToLogin)
        {
            try
            {
                MySqlCommand cmdAuthenticate = this._connection.CreateCommand();
                cmdAuthenticate.CommandText = "select * from users where " +
                    "(email = @email and passwrd = sha1(@pwd)) or" +
                    "(username = @user and passwrd = sha1(@pwd))";
                cmdAuthenticate.Parameters.AddWithValue("email", userDataToLogin.EMail);
                cmdAuthenticate.Parameters.AddWithValue("user", userDataToLogin.Username);
                cmdAuthenticate.Parameters.AddWithValue("pwd", userDataToLogin.Password);

                using (MySqlDataReader reader = cmdAuthenticate.ExecuteReader())
                {
                    if (reader.HasRows)
                    {
                        reader.Read();

                        return new User
                        {
                            ID = Convert.ToInt32(reader["id"]),
                            Firstname = Convert.ToString(reader["firstname"]),
                            Lastname = Convert.ToString(reader["lastname"]),
                            BirthDate = reader["birthdate"] != DBNull.Value ? Convert.ToDateTime(reader["birthdate"]) : (DateTime?)null,
                            Gender = (Gender)Convert.ToInt32(reader["gender"]),
                            EMail = Convert.ToString(reader["email"]),
                            Username = Convert.ToString(reader["username"]),
                        };
                    }

                }
            }

            catch (Exception)
            {
                throw;
            }

            return null;
        }

        public bool ChangePersonData(int userIdToChange, User newUserData)
        {
            if (this._connection == null || this._connection.State != ConnectionState.Open)
            {
                return false;
            }

            try
            {
                MySqlCommand cmdChangeUserData = this._connection.CreateCommand();
                cmdChangeUserData.CommandText = "update users set firstname=@firstname, lastname=@lastname, email=@email, username=@username, birthdate=@birthdate, passwrd=sha1(@passwrd) where id=@id";
                cmdChangeUserData.Parameters.AddWithValue("firstname", newUserData.Firstname);
                cmdChangeUserData.Parameters.AddWithValue("lastname", newUserData.Lastname);
                cmdChangeUserData.Parameters.AddWithValue("email", newUserData.EMail);
                cmdChangeUserData.Parameters.AddWithValue("username", newUserData.Username);
                cmdChangeUserData.Parameters.AddWithValue("birthdate", newUserData.BirthDate);
                cmdChangeUserData.Parameters.AddWithValue("passwrd", newUserData.Password);
                cmdChangeUserData.Parameters.AddWithValue("id", userIdToChange);

                return cmdChangeUserData.ExecuteNonQuery() == 1;
            }
            catch (MySqlException)
            {
                throw;
            }
        }

        public bool Remove(int idToDelete)
        {
            if (this._connection == null || this._connection.State != ConnectionState.Open)
            {
                return false;
            }

            try
            {
                MySqlCommand cmdDelete = this._connection.CreateCommand();
                cmdDelete.CommandText = "delete from users where id = @idToDelete";
                cmdDelete.Parameters.AddWithValue("idToDelete", idToDelete);

                return cmdDelete.ExecuteNonQuery() == 1;
            }
            catch (MySqlException)
            {
                throw;
            }
        }

    }
}