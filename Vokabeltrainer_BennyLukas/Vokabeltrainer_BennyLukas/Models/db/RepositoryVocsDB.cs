﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MySql.Data.MySqlClient;
using System.Data;

namespace Vokabeltrainer_BennyLukas.Models.db
{
    public class RepositoryVocsDB : IRepositoryVocs
    {
        private string _connectionString = "Server = localhost; Database = swp_vocember; Uid = root; Pwd = ''";
        private MySqlConnection _connection;

        public void Open()
        {
            if (this._connection == null)
            {
                this._connection = new MySqlConnection(this._connectionString);
            }
            if (this._connection.State != ConnectionState.Open)
            {
                this._connection.Open();
            }
        }

        public void Close()
        {
            if ((this._connection != null) && (this._connection.State == ConnectionState.Open))
            {
                this._connection.Close();
            }
        }

        public List<Word> GetAllPublicWords()
        {
            List<Word> words = new List<Word>();

            try
            {
                MySqlCommand cmdAllWords = this._connection.CreateCommand();
                cmdAllWords.CommandText = "select * from vocabs";

                using (MySqlDataReader reader = cmdAllWords.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        words.Add(new Word()
                        {
                            ID = Convert.ToInt32(reader["id"]),
                            Vocab = Convert.ToString(reader["vocab"]),
                            Translation = Convert.ToString(reader["translation"]),
                            Note = Convert.ToString(reader["note"]),
                            CreationDate = reader["creationdate"] != DBNull.Value ? Convert.ToDateTime(reader["creationdate"]) : (DateTime?)null,
                            Genre=Convert.ToString(reader["genre"]),
                        });
                    }
                }
                return words.Count == 0 ? null : words;
            }

            catch (Exception)
            {
                throw;
            }
        }

        public bool Insert(Word wordToInsert)
        {
            if (wordToInsert == null)
            {
                return false;
            }

            try
            {
                MySqlCommand cmdInsert = this._connection.CreateCommand();
                cmdInsert.CommandText = "insert into vocabs values(null, @voc, @tra, @not, @cre, @gen)";
                cmdInsert.Parameters.AddWithValue("voc", wordToInsert.Vocab);
                cmdInsert.Parameters.AddWithValue("tra", wordToInsert.Translation);
                cmdInsert.Parameters.AddWithValue("not", wordToInsert.Note);
                cmdInsert.Parameters.AddWithValue("cre", wordToInsert.CreationDate);
                cmdInsert.Parameters.AddWithValue("gen", wordToInsert.Genre);

                return cmdInsert.ExecuteNonQuery() == 1;
            }

            catch (Exception)
            {
                throw;
            }
        }

        public bool Remove(int idToDelete)
        {
            if (this._connection == null || this._connection.State != ConnectionState.Open)
            {
                return false;
            }

            try
            {
                MySqlCommand cmdDelete = this._connection.CreateCommand();
                cmdDelete.CommandText = "delete from vocabs where id = @idToDelete";
                cmdDelete.Parameters.AddWithValue("idToDelete", idToDelete);

                return cmdDelete.ExecuteNonQuery() == 1;
            }
            catch (MySqlException)
            {
                throw;
            }
        }

        //connection to user:

        public bool InsertUserVocableRealtion(UserVocabs uvToInsert)
        {
            if (uvToInsert == null)
            {
                return false;
            }

            try
            {
                MySqlCommand cmdInsert = this._connection.CreateCommand();
                cmdInsert.CommandText = "insert into user_vocabs values(@uid, @vid)";
                cmdInsert.Parameters.AddWithValue("uid", uvToInsert.UserID);
                cmdInsert.Parameters.AddWithValue("vid", uvToInsert.VocabID);

                return cmdInsert.ExecuteNonQuery() == 1;
            }

            catch (Exception)
            {
                throw;
            }
        }

        public bool RemoveUserVocableRelation(UserVocabs uvToRemove)
        {
            if (this._connection == null || this._connection.State != ConnectionState.Open)
            {
                return false;
            }

            try
            {
                MySqlCommand cmdDelete = this._connection.CreateCommand();
                cmdDelete.CommandText = "delete from user_vocabs where userid = @uid and vocabid = @vid";
                cmdDelete.Parameters.AddWithValue("uid", uvToRemove.UserID);
                cmdDelete.Parameters.AddWithValue("vid", uvToRemove.VocabID);

                return cmdDelete.ExecuteNonQuery() == 1;
            }
            catch (MySqlException)
            {
                throw;
            }
        }

        public List<Word> GetAllUserVocabs(int uidToFind)
        {
            if (this._connection == null || this._connection.State != ConnectionState.Open)
            {
                return null;
            }

            List<Word> words = new List<Word>();

            try
            {
                MySqlCommand cmdAllUserWords = this._connection.CreateCommand();
                cmdAllUserWords.CommandText = "select * from vocabs join user_vocabs on(id = vocabid) where userid = @uid;";
                cmdAllUserWords.Parameters.AddWithValue("uid", uidToFind);
                //HIER WEITER


                using (MySqlDataReader reader = cmdAllUserWords.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        words.Add(new Word()
                        {
                            ID = Convert.ToInt32(reader["id"]),
                            Vocab = Convert.ToString(reader["vocab"]),
                            Translation = Convert.ToString(reader["translation"]),
                            Note = Convert.ToString(reader["note"]),
                            CreationDate = reader["creationdate"] != DBNull.Value ? Convert.ToDateTime(reader["creationdate"]) : (DateTime?)null,
                            Genre = Convert.ToString(reader["genre"]),
                        });
                    }
                }
                return words.Count == 0 ? null : words;
            }

            catch (Exception)
            {
                throw;
            }
        }

        public int GetLastVocabID()
        {
            if (this._connection == null || this._connection.State != ConnectionState.Open)
            {
                return -1;
            }

            try
            {
                MySqlCommand cmdID = this._connection.CreateCommand();
                cmdID.CommandText = "select id from vocabs order by id desc limit 1";

                using (MySqlDataReader reader = cmdID.ExecuteReader())
                {
                    if (reader.HasRows)
                    {
                        reader.Read();
                        int res = Convert.ToInt32(reader["ID"]);
                        return res;
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }
            return -1;
        }

        public Word GetUserVocab(int uidToFind)
        {
            if (this._connection == null || this._connection.State != ConnectionState.Open)
            {
                return null;
            }

            try
            {
                MySqlCommand cmdGetRegVoc = this._connection.CreateCommand();
                cmdGetRegVoc.CommandText = "select * from vocabs where id=@id";
                cmdGetRegVoc.Parameters.AddWithValue("id", uidToFind);

                using (MySqlDataReader reader = cmdGetRegVoc.ExecuteReader())
                {
                    if (reader.HasRows)
                    {
                        reader.Read();
                        return new Word
                        {
                            ID = Convert.ToInt32(reader["id"]),
                            Vocab = Convert.ToString(reader["vocab"]),
                            Translation = Convert.ToString(reader["translation"]),
                            Note = Convert.ToString(reader["note"]),
                            CreationDate = reader["creationdate"] != DBNull.Value ? Convert.ToDateTime(reader["creationdate"]) : (DateTime?)null,
                            Genre = Convert.ToString(reader["genre"]),
                        };
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }
            return null;
        }

        ////////////////////////////////////////////////////////////////////////
        //public Word GetRegisteredWord(int idToFind)
        //{
        //    throw new NotImplementedException();
        //}

        //public bool ChangeWordData(int idToChange, Word newWordData)
        //{
        //    throw new NotImplementedException();
        //}
    }
}
