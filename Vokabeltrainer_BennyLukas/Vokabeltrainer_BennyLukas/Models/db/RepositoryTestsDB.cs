﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using MySql.Data.MySqlClient;
using Vokabeltrainer_BennyLukas.Models;


namespace Vokabeltrainer_BennyLukas.Models.db
{
    public class RepositoryTestsDB : IRepositoryTests
    {

        private string _connectionString = "Server = localhost; Database = swp_vocember; Uid = root; Pwd = ''";
        private MySqlConnection _connection;

        public void Open()
        {
            if (this._connection == null)
            {
                this._connection = new MySqlConnection(this._connectionString);
            }
            if (this._connection.State != ConnectionState.Open)
            {
                this._connection.Open();
            }
        }

        public void Close()
        {
            if ((this._connection != null) && (this._connection.State == ConnectionState.Open))
            {
                this._connection.Close();
            }
        }
    }
}