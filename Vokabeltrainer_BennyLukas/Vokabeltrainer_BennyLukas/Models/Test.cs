﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Vokabeltrainer_BennyLukas.Models
{
    public class Test
    {
        public List <string> TestAnswers { get; set; }
        public List <Word> TestSolution { get; set; }
        public List<bool> TestCorrected { get; set; }

        public Test() : this(new List<string>(),new List<Word>(), new List<bool>()) { }
        public Test(List<string> testAnswers,List<Word> testSolution, List<bool> testCorrected)
        {
            this.TestAnswers = testAnswers;
            this.TestSolution = testSolution;
            this.TestCorrected = testCorrected;
        }
    }
}