﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Vokabeltrainer_BennyLukas.Models
{
    public class Word
    {
        public int ID { get; set; }
        public string Vocab { get; set; }
        public string Translation { get; set; }
        public string Note { get; set; }
        public DateTime? CreationDate { get; set; }
        public string Genre { get; set; }

        public Word() : this(0, "", "", "", null, "") { }
        public Word(int id, string vocab, string translation, string note, DateTime? creationDate, string genre)
        {
            this.ID = id;
            this.Vocab = vocab;
            this.Translation = translation;
            this.Note = note;
            this.CreationDate = creationDate;
            this.Genre = genre;
        }

    }
}