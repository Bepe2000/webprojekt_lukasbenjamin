﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Vokabeltrainer_BennyLukas.Models
{
    public enum Gender
    {
        male, female, notSpecified
    }
    public class User
    {
        public int ID { get; set; }
        public string Firstname { get; set; }
        public string Lastname { get; set; }
        public DateTime? BirthDate { get; set; }
        public Gender Gender { get; set; }
        public string EMail { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }

        public User() : this(-1, "", "", null, Gender.notSpecified, "", "", "") { }
        public User(int id, string fn, string ln, DateTime? bd, Gender gnd, string em,
            string un, string pw)
        {
            this.ID = id;
            this.Firstname = fn;
            this.Lastname = ln;
            this.BirthDate = bd;
            this.Gender = gnd;
            this.EMail = em;
            this.Username = un;
            this.Password = pw;
        }

    }
}