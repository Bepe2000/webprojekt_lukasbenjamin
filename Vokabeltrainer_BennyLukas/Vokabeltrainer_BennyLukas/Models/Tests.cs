﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Vokabeltrainer_BennyLukas.Models
{
    public class Tests
    {
        public int UserId { get; set; }
        public int TestId { get; set; }
        public string TestName { get; set; }
        public List<Word> TestWords { get; set; }
        public List<string> TestAnswers { get; set; }
        public int CorrectW { get; set; }
        public int FalseW { get; set; }

        public Tests() : this(0, 0, "", new List<Word>(), 0, 0 ) { }
        public Tests(int userId, int testId, string testName, List<Word> testWords, int correctW, int falseW)
        {
            this.UserId = userId;
            this.TestId = testId;
            this.TestName = testName;
            this.TestWords = testWords;
            this.CorrectW = correctW;
            this.FalseW = falseW;

        }
    }
}