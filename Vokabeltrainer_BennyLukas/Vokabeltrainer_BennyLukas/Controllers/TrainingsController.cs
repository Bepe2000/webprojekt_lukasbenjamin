﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Vokabeltrainer_BennyLukas.Models;
using Vokabeltrainer_BennyLukas.Models.db;

namespace Vokabeltrainer_BennyLukas.Controllers
{
    public class TrainingsController : Controller
    {
        // GET: Trainings
        [HttpGet]
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Index(int? Auswahl, bool? isChecked)
        {
            //if(Usert != null)
            //{
            //    List<string> vs = Usert.TestAnswers;
            //    List<Word> ws = Usert.TestSolution;
            //    return Test(Usert, null);
            //}

            bool? check = isChecked;
            if ((Auswahl == null) || (Auswahl != 0))
            {
                int? Selected = Auswahl;
                    if ((Selected > 0) && (Selected <= 5) && (check == true))
                    {
                        List<Word> AllWords = new List<Word>();
                        List<Word> UserWords = new List<Word>();
                        Test Usertest = new Test(new List<string>(), new List<Word>(), new List<bool>());
                        RepositoryVocsDB repVocs = new RepositoryVocsDB();
                        User u = (User)Session["loggedInUser"];
                        repVocs.Open();

                        AllWords = repVocs.GetAllPublicWords();
                        foreach (Word w in repVocs.GetAllUserVocabs(u.ID))
                        {
                            AllWords.Add(w);
                        }

                        Usertest.TestSolution = (Wordsgenerator(Selected, AllWords));
                         Session["UserTest"] = Usertest;
                    
                        return View("Test");

                    }
                    else if ((Selected > 0) && (Selected <= 5))
                    {
                        List<Word> NormalWords = new List<Word>();
                    Test Usertest = new Test(new List<string>(), new List<Word>(), new List<bool>());
                    RepositoryVocsDB repVocs = new RepositoryVocsDB();
                        repVocs.Open();

                        NormalWords = repVocs.GetAllPublicWords();
                        Random max = new Random();

                       Usertest.TestSolution = Wordsgenerator(Selected, NormalWords);
                    //next View Ausgabe + Abtesten
                    Session["UserTest"] = Usertest;
                    return View("Test");

                    }
                    else
                    {
                        return View("Message", new Message("1-5", "Bitte geben Sie eine Zahl zwischen 1-5 ein"));
                    }
            }
            else
            {
                return View("Message", new Message(":-(", "Sie haben leider keine Auswahl getroffen"));
            }

        }



        [HttpGet]
        public ActionResult Test()
        {

            Test UserT;

            UserT = (Test)Session["TestResult"];
            if (UserT != null)
            {
                return View("Test",UserT);
            }
            else
            {
                return View("Message", new Message("Fehler", "Es ist leider ein Problem aufgetreten bitte wiederholen Sie den vorgang nochmal"));
            }
        }

        [HttpGet]
        public ActionResult Test(string TestResult)
        {
            
            //if(TestResult == null)
            //{
            //    return View("Index", "Trainings");
            //}
            //TestResult.TestAnswers = new List<string>();
            //TestResult.TestSolution = new List<Word>();
           return View();
        }

        public List<Word> Wordsgenerator(int? Selected, List<Word> Vocabs)
        {
            if((Selected == 0) || (Vocabs == null)||(Selected == null))
            {
                return null;
            }

            List<Word> select = new List<Word>();
            Random rnd = new Random();
            bool AddW = true;
            int rndNum = rnd.Next(0, Vocabs.Count());
            do
            {
                if (select.Count() == 0)
                {
                    select.Add(Vocabs[rndNum]);
                }

                foreach(Word w in select)
                {
                    if (Vocabs[rndNum].ID == w.ID)
                    {
                        AddW = false;
                    }
                }
                if(AddW == true)
                {
                    select.Add(Vocabs[rndNum]);
                }
                else if(AddW == false)
                {
                    rndNum = rnd.Next(0, Vocabs.Count());
                    AddW = true;
                }
            } while (select.Count() != Selected);

            return select;
        }

        public bool CheckResult(string Solution, Word WordsToProve)
        {
            if((Solution == null) || (WordsToProve == null))
            {
                return false;
            }

            string txt = Solution;

            txt.Trim();
            if(WordsToProve.Vocab == txt)
            {
                return true;

            }
            else
            {
                return false;
            }
        }
        
        public List<bool> Results(List<string> Solutions, List<Word> WordsToProve)
        {
            if((Solutions == null)||(WordsToProve == null))
            {
                return null;
            }
            List<bool> Result = new List<bool>();

            foreach(String s in Solutions)
            {
                foreach(Word w in WordsToProve)
                {
                    Result.Add(CheckResult(s, w));
                }
            }
            return Result;
        }
    }
}
    