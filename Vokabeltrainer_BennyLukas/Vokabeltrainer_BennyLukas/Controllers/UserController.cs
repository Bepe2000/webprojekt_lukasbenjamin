﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Vokabeltrainer_BennyLukas.Models;
using Vokabeltrainer_BennyLukas.Models.db;
using MySql.Data.MySqlClient;

namespace Vokabeltrainer_BennyLukas.Controllers
{
    public class UserController : Controller
    {
        IRepositoryUsers repUsers;

        public ActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public ActionResult Login()
        {
            User u = new User();
            return View(u);
        }

        [HttpPost]
        public ActionResult Login(User personDataFromForm)
        {
            if ((personDataFromForm.Username != null) && (personDataFromForm.Password != null))
            {
                repUsers = new RepositoryUsersDB();
                repUsers.Open();

                try
                {
                    User person = repUsers.Authenticate(personDataFromForm);
                    if (person != null)
                    {
                        Session["loggedInUser"] = person;
                        string hello = "Hallo " + person.Firstname + "!";
                        return View("Message", new Message("Login", hello));
                    }
                    else
                    {
                        return View("Message", new Message("Login", "Diese Daten existieren nicht!"));
                    }
                }

                catch (MySqlException)
                {
                    return View("Message", new Message("Datenbankfehler", "Es gibt zur Zeit ein Problem mit der Datenbank!"));
                }
                catch (Exception)
                {
                    return View("Message", new Message("Allgemeiner Fehler", "Es ist ein allgemeiner Fehler aufgetreten!"));
                }

                finally
                {
                    repUsers.Close();
                }

            }
            else
            {
                return RedirectToAction("Login");
            }
        }
        
        public ActionResult Logout()
        {
            Session["loggedInUser"] = null;
            return View("Message", new Message(null, "Abmelden erfolgreich!"));
        }

        [HttpGet]
        public ActionResult Registration()
        {
            User u = new User();
            return View(u);
        }

        [HttpPost]
        public ActionResult Registration(User personDataFromForm)
        {
            if (personDataFromForm != null)
            {
                ValidationForRegistrationForm(personDataFromForm);
                if (ModelState.IsValid)
                {
                    repUsers = new RepositoryUsersDB();
                    repUsers.Open();
                    try
                    {
                        if (repUsers.Insert(personDataFromForm))
                        {
                            return View("Message", new Message("Registrierung", "Registrierung erfolgreich!"));
                        }
                        else
                        {
                            return View("Message", new Message("Registrierung", "Registrierung nicht erfolgreich!"));
                        }
                    }

                    catch (MySqlException)
                    {
                        return View("Message", new Message("Datenbankfehler", "Es gibt zur Zeit ein Problem mit der Datenbank!"));
                    }

                    catch (Exception)
                    {
                        return View("Message", new Message("Allgemeiner Fehler", "Es ist ein allgemeiner Fehler aufgetreten!"));
                    }

                    finally
                    {
                        repUsers.Close();
                    }
                }

                else
                {
                    return View(personDataFromForm);
                }
            }
            else
            {
                return RedirectToAction("Registration");
            }
        }

        [HttpGet]
        public ActionResult Update()
        {
            User u = new User();
            if (Session["loggedInUser"] != null)
            {
                u = (User)Session["loggedInUser"];
            }
            else
            {
                u = null;
            }
            return View(u);
        }

        [HttpPost]
        public ActionResult Update(User personDataFromForm)
        {
            repUsers = new RepositoryUsersDB();
            bool noFail = true;
            if (personDataFromForm != null)
            {
                try
                {
                    repUsers.Open();
                    ValidationForRegistrationForm(personDataFromForm);
                    if (ModelState.IsValid)
                    {
                        User idUser = (User)Session["loggedInUser"];
                        personDataFromForm.ID = idUser.ID;
                        noFail = repUsers.ChangePersonData(personDataFromForm.ID, personDataFromForm);
                    }
                }
                catch (MySqlException)
                {
                    return View("Message", new Message("Datenbankfehler", "Es gibt zur Zeit ein Problem mit der Datenbank."));
                }
                catch (Exception)
                {
                    return View("Message", new Message("Allgemeiner Fehler", "Es ist ein allgemeiner Fehler aufgetreten."));
                }
                finally
                {
                    repUsers.Close();
                }
                if ((ModelState.IsValid) && (noFail == true))
                {
                    Session["loggedInUser"] = personDataFromForm;
                    return View("Message", new Message("Geändert", "Änderungen wurden durchgeführt."));
                }
                else
                {
                    return View(personDataFromForm);
                }
            }
            else
            {
                return RedirectToAction("Update");
            }
        }

        private void ValidationForRegistrationForm(User person)
        {
            if (person.Firstname == null)
            {
                ModelState.AddModelError("Firstname", "Vorname ist ein Pflichtfeld!");
            }

            if (person.Lastname == null)
            {
                ModelState.AddModelError("Lastname", "Nachname ist ein Pflichtfeld!");
            }
            if ((person.BirthDate >= DateTime.Today)||(person.BirthDate==null))
            {
                ModelState.AddModelError("BirthDate", "Das angegebene Alter ist ungültig!");
            }
            if (person.EMail == null)
            {
                ModelState.AddModelError("EMail", "E-Mail Adresse ist ein Pflichtfeld!");
            }
            if ((person.Username != null) && (person.Username.Trim().Length < 4))
            {
                ModelState.AddModelError("Username", "Der Benutzername muss mind. 4 Zeichen lang sein.");
            }
            if ((person.Password == null) || (person.Password.Length < 4))
            {
                ModelState.AddModelError("Password", "Das Passwort muss mind. 4 Zeichen lang sein!");
            }

        }

    }
}