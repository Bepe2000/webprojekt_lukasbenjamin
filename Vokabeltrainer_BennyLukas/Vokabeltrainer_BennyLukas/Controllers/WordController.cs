﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Vokabeltrainer_BennyLukas.Models;
using Vokabeltrainer_BennyLukas.Models.db;
using MySql.Data.MySqlClient;
namespace Vokabeltrainer_BennyLukas.Controllers
{
    public class WordController : Controller
    {
        IRepositoryVocs repVocs;

        public ActionResult Index()
        {
            repVocs = new RepositoryVocsDB();
            repVocs.Open();

            try
            {
                return View("Index", repVocs.GetAllPublicWords());
            }
            catch (Exception)
            {
                return View("Message", new Message("Fehler", "Es ist ein Fehler aufgetreten!"));
            }
            finally
            {
                repVocs.Close();
            }
        }

        public ActionResult RegisteredUserVocablist()
        {
            repVocs = new RepositoryVocsDB();
            repVocs.Open();

            try
            {
                if (Session["loggedInUser"] != null)
                {
                    User u = (User)Session["loggedInUser"];
                    return View("RegisteredUserVocabList",repVocs.GetAllUserVocabs(u.ID));
                }

                else
                {
                    return View("Message", new Message("Fehler", "Sie müssen angemeldet sein, um Ihre persönliche Liste zu sehen!"));
                }
            }

            catch (Exception)
            {
                return View("Message", new Message("Fehler", "Es ist ein Fehler aufgetreten!"));
            }

            finally
            {
                repVocs.Close();
            }
        }

        [HttpGet]
        public ActionResult AddRegisteredUserVocab()
        {
            if (Session["loggedInUser"] != null)
            {
                Word w = new Word();
                return View(w);
            }
            else
            {
                return View("Message", new Message("Fehler", "Sie müssen angemeldet sein, um Vokabeln hinzuzufügen!"));
            }
        }

        [HttpPost]
        public ActionResult AddRegisteredUserVocab(Word wordToAdd)
        {
            if (wordToAdd != null)
            {
                ValidationForAddVocab(wordToAdd);
                if (ModelState.IsValid)
                {
                    repVocs = new RepositoryVocsDB();
                    repVocs.Open();
                    User u = (User)Session["loggedInUser"];
                    //UserVocabs uv = new UserVocabs(u.ID, wordToAdd.ID);
                    try
                    {
                        bool insertOK = repVocs.Insert(wordToAdd);
                        // die ID aus der DB-Tabelle des gerade eingefügten vokabels ermitteln
                        int lastVocabID = repVocs.GetLastVocabID();
                        // in die zwischentabelle die richtigen werte eintragen
                        UserVocabs uv = new UserVocabs(u.ID, lastVocabID);
                        bool relationOK = repVocs.InsertUserVocableRealtion(uv);

                        if (insertOK && relationOK)
                        {
                            return View("Message", new Message("Vokabel hinzufügen", "Eintrag erfolgreich!"));
                        }
                        else
                        {
                            return View("Message", new Message("Vokabel hinzufügen", "Eintrag nicht erfolgreich!"));
                        }
                    }

                    catch (MySqlException)
                    {
                        return View("Message", new Message("Datenbankfehler", "Es gibt zur Zeit ein Problem mit der Datenbank!"));
                    }

                    catch (Exception)
                    {
                        return View("Message", new Message("Allgemeiner Fehler", "Es ist ein allgemeiner Fehler aufgetreten!"));
                    }

                    finally
                    {
                        repVocs.Close();
                    }
                }

                else
                {
                    return View(wordToAdd);
                }
            }
            else
            {
                return RedirectToAction("Index");
            }
        }

        //[HttpGet]
        //public ActionResult DeleteRegisteredUserVocab()
        //{
        //    if (Session["loggedInUser"] != null)
        //    {
        //        Word w = new Word();
        //        return View(w);
        //    }
        //    else
        //    {
        //        return View("Message", new Message("Fehler", "Sie müssen angemeldet sein, um Vokabeln zu löschen!"));
        //    }
        //}

        [HttpGet]
        public ActionResult DeleteRegisteredUserVocab(int ID)
        {
            if (ID >= 0)
            {
                repVocs = new RepositoryVocsDB();
                repVocs.Open();
                User u = (User)Session["loggedInUser"];
                try
                {
                    // remove uv entry
                    UserVocabs uv = new UserVocabs(u.ID, ID);
                    bool deleteOK = repVocs.RemoveUserVocableRelation(uv);

                    if (deleteOK)
                    {
                        return View("Message", new Message("Vokabel löschen", "Eintrag erfolgreich entfernt!"));
                    }
                    else
                    {
                        return View("Message", new Message("Vokabel löschen", "Eintrag konnte nicht entfernt werden!"));
                    }
                }

                catch (MySqlException)
                {
                    return View("Message", new Message("Datenbankfehler", "Es gibt zur Zeit ein Problem mit der Datenbank!"));
                }

                catch (Exception)
                {
                    return View("Message", new Message("Allgemeiner Fehler", "Es ist ein allgemeiner Fehler aufgetreten!"));
                }

                finally
                {
                    repVocs.Close();
                }
            }

            else
            {
                return RedirectToAction("Index");
            }
        }

        //[HttpGet]
        //public ActionResult SearchVocab()
        //{
        //    if (Session["loggedInUser"] != null)
        //    {
        //        return View("DeleteRegisteredUserVocab", new Word());
        //    }
        //    else
        //    {
        //        return View("Message", new Message("Fehler", "Sie müssen angemeldet sein, um Vokabeln zu löschen!"));
        //    }
        //}

        //[HttpPost]
        //public ActionResult SearchVocab(int ID)
        //{
        //    if(ID < 0)
        //    {
        //        return RedirectToAction("Index");
        //    }
        //    else
        //    {
        //        Word wordFromDB = new Word();

        //        return View("DeleteRegisteredUserVocab", wordFromDB);
        //    }
        //}
        

        private void ValidationForAddVocab(Word word)
        {
            if (word.Vocab == null)
            {
                ModelState.AddModelError("Vocab", "'Vokabel' ist ein Pflichtfeld!");
            }
            if (word.Translation == null)
            {
                ModelState.AddModelError("Translation", "'Übersetzung' ist ein Pflichtfeld!");
            }
            if (word.Genre == null)
            {
                ModelState.AddModelError("Genre", "'Genre' ist ein Pflichtfeld!");
            }
            if ((word.CreationDate > DateTime.Today) || (word.CreationDate == null))
            {
                ModelState.AddModelError("CreationDate", "Das angegebene Datum ist ungültig!");
            }
        }
    }
}