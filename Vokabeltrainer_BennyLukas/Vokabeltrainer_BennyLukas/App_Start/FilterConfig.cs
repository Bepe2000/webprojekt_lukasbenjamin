﻿using System.Web;
using System.Web.Mvc;

namespace Vokabeltrainer_BennyLukas
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }
    }
}
