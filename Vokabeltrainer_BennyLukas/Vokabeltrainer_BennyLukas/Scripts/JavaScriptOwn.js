﻿function responsiveMenu() {
    var visible = document.getElementsByClassName('navSmall')[0];
    var notVisible = document.getElementsByClassName('navBig')[0];

    if (visible) {
        visible.className = "navBig";
    }
    if (notVisible) {
        notVisible.className = "navSmall";
    }
}